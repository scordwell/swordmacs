(require 'cl-lib)
(require 'ert)
(cl-eval-when (load)
  (require 'swordmacs)
  (require 'swordmacs-testing))

(defmacro swordmacs-def-doctest (var)
  "Define a test to check that a Swordmacs function VAR has documentation."
  (let ((test-name (intern (concat "swordmacs-" var "-has-documentation")))
        (function-name (intern (concat "swordmacs-" var))))
    `(ert-deftest ,test-name ()
       (let ((doc-string (documentation (quote ,function-name))))
         (should (and (not (equal "" doc-string))
                      (not (eq nil doc-string))))))))

(defmacro swordmacs-def-bad-ptr-test (var)
  (let ((test-name (intern (concat "swordmacs-" var
                                   "-signals-error-with-invalid-user-ptr")))
        (function-name (intern (concat "swordmacs-" var))))
    `(ert-deftest ,test-name ()
       (should-error (,function-name (swordmacs-testing-null-user-ptr))
                     :type 'error))))

(defmacro with-swordmacs-module (name &rest forms)
  (declare (indent 1))
  `(let ((swordmacs-test-module (swordmacs-module-open ,name)))
     (swordmacs-module-position swordmacs-test-module 'top)
     ,@forms
     (swordmacs-module-position swordmacs-test-module 'top)))

(ert-deftest list-modules-returns-list-of-info-about-modules ()
  (should (eq 't
              (cl-every (lambda (x) (= 3 (length x)))
                        (swordmacs-list-modules)))))

(swordmacs-def-doctest "list-modules")

(ert-deftest swordmacs-module-open-returns-user-ptr-type ()
  (with-swordmacs-module "KJV"
    (should (eq 'user-ptr
                (type-of swordmacs-test-module)))))

(swordmacs-def-doctest "module-open")

(ert-deftest swordmacs-module-open-commentary-signals-error ()
  (with-swordmacs-module "KJV"
    (should-error (swordmacs-module-open "MHC")
                  :type 'error)))

(ert-deftest swordmacs-module-open-signals-error-with-incorrect-name ()
  (should-error (swordmacs-module-open "test invalid name")
                :type 'error))

(ert-deftest swordmacs-module-open-signals-error-with-incorrect-chinese-name ()
  (should-error (swordmacs-module-open "中文")
                :type 'error))

(ert-deftest swordmacs-module-name-returns-the-name ()
  (let ((name "KJV"))
    (with-swordmacs-module name
      (should (equal name
                     (swordmacs-module-name swordmacs-test-module))))))

(swordmacs-def-doctest "module-name")

(swordmacs-def-bad-ptr-test "module-name")

(ert-deftest swordmacs-module-key-returns-unabbreviated-title ()
  (with-swordmacs-module "KJV"
    (should (equal "Genesis 1:1"
                   (swordmacs-module-key swordmacs-test-module)))))

(ert-deftest swordmacs-module-key-set-sets-the-key ()
  (with-swordmacs-module "KJV"
    (should (equal "John 3:16"
                   (swordmacs-module-key swordmacs-test-module "jn.3.16")))))

(ert-deftest swordmacs-module-key-set-with-wrong-type-argument ()
  (with-swordmacs-module "KJV"
    (should-error (swordmacs-module-key swordmacs-test-module 'John-3-16)
                  :type 'wrong-type-argument)))

(swordmacs-def-doctest "module-key")

(swordmacs-def-bad-ptr-test "module-key")

(ert-deftest swordmacs-module-text-strip-returns-string-type ()
  (with-swordmacs-module "KJV"
    (should (eq 'string
                (type-of (swordmacs-module-text-strip swordmacs-test-module))))))

(swordmacs-def-doctest "module-text-strip")

(swordmacs-def-bad-ptr-test "module-text-strip")

(ert-deftest swordmacs-module-position-bottom-is-at-end ()
  (with-swordmacs-module "KJV"
    (swordmacs-module-position swordmacs-test-module 'top)
    (swordmacs-module-position swordmacs-test-module 'bottom)
    (should (equal "Revelation of John 22:21"
                   (swordmacs-module-key swordmacs-test-module)))))

(ert-deftest swordmacs-module-position-top-is-at-start ()
  (with-swordmacs-module "KJV"
    (swordmacs-module-position swordmacs-test-module 'bottom)
    (swordmacs-module-position swordmacs-test-module 'top)
    (should (equal "Genesis 1:1"
                   (swordmacs-module-key swordmacs-test-module)))))

(ert-deftest swordmacs-module-position-returns-nil ()
  (with-swordmacs-module "KJV"
    (should (eq nil
                (swordmacs-module-position swordmacs-test-module 'top)))))

(ert-deftest swordmacs-module-position-invalid-value-signals-error ()
  (with-swordmacs-module "KJV"
    (should-error (swordmacs-module-position swordmacs-test-module 'test-symbol)
                  :type 'error)))

(swordmacs-def-doctest "module-position")

(swordmacs-def-bad-ptr-test "module-position")

(ert-deftest swordmacs-module-inc-returns-nil ()
  (with-swordmacs-module "KJV"
    (should (eq nil
                (swordmacs-module-inc swordmacs-test-module)))))

(ert-deftest swordmacs-module-inc-increments-the-key-by-one ()
  (with-swordmacs-module "KJV"
    (swordmacs-module-inc swordmacs-test-module)
    (should (equal "Genesis 1:2"
                   (swordmacs-module-key swordmacs-test-module)))))

(swordmacs-def-doctest "module-inc")

(swordmacs-def-bad-ptr-test "module-inc")
