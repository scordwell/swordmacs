;;; elbible.el --- Read the Bible using the Sword library

;; Copyright (C) 2017 Steven Cordwell

;; Author: Steven Cordwell <steve.cordwell@gmail.com>
;; Created: 24 Jan 2017
;; Version: 0.1

;; Keywords: extensions, wp
;; Homepage:

;; This file is not part of GNU Emacs.

;; This file is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Elbible is a front-end for the the Sword Project's SWORD library.
;; It allows you to read and manage Sword modules from within Emacs.
;; The Sword Project's homepage is http://crosswire.org/sword/index.jsp.

;;; Code:

(require 'swordmacs)

(provide 'elbible)

;;; elbible.el ends here
