
EMACS := /usr/bin/emacs-25

LDFLAGS := -shared
LIBS := -lsword
CFLAGS := -std=c99 -s -Wall -Wextra -O3 -fPIC
CXXFLAGS := -std=c++14 -s -Wall -Wextra -O3 -fPIC -I/usr/include/sword

all: swordmacs.so elbible.elc

swordmacs.so: swordmacs.cpp
	$(CXX) -shared $(CXXFLAGS) -o $@ $^ $(LIBS)

swordmacs-testing.so: swordmacs-testing.c
	$(CC) -shared $(CFLAGS) -o $@ $^

elbible.elc: elbible.el
	$(EMACS) -batch -L . -f batch-byte-compile $<

test-swordmacs.elc: test-swordmacs.el swordmacs.so swordmacs-testing.so
	$(EMACS) -batch -L . -f batch-byte-compile $<

test-elbible.elc: test-elbible.el elbible.elc
	$(EMACS) -batch -L . -f batch-byte-compile $<

check: check-swordmacs check-elbible check-elbible-doc

check-swordmacs: test-swordmacs.elc
	$(EMACS) -batch -L . -l ert -l $< -f ert-run-tests-batch-and-exit

check-elbible: test-elbible.elc swordmacs.so
	$(EMACS) -batch -L . -l ert -l $< -f ert-run-tests-batch-and-exit

check-elbible-doc: elbible.el
	$(EMACS) -batch --eval "(checkdoc-file \"$<\")"

clean:
	$(RM) swordmacs.so elbible.elc test-swordmacs.elc test-elbible.elc swordmacs-testing.so

.PHONY: all check check-swordmacs check-elbible check-elbible-doc clean
