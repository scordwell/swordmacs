/* swordmacs.cpp - Libsword wrapper module for GNU Emacs.

   Copyright (C) 2016 Steven Cordwell.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#include <algorithm>
#include <array>
#include <cstddef>
#include <cstring>
#include <exception>
#include <string>
#include <vector>

#include <swmgr.h>
#include <swmodule.h>
#include <swtext.h>

#include "emacs-module.h"

#define DOC_LIST_MODULES                                                 \
  "(swordmacs-modules-list)\n"                                           \
  "\n"                                                                   \
  "List all the installed Sword modules."

#define DOC_MODULE_OPEN                                                  \
  "(swordmacs-module-open MANAGER NAME)\n"                               \
  "\n"                                                                   \
  "Get a handle to a Sword module.\n"                                    \
  "\n"                                                                   \
  "MANAGER is a user-ptr returned from swordmacs-manager-open and\n"     \
  "NAME is a string of the Sword module's name.\n"                       \
  "A user-ptr that points to the module is returned."

#define DOC_MODULE_NAME                                                  \
  "(swordmacs-module-name MODULE)\n"                                     \
  "\n"                                                                   \
  "Get the name of Sword module MODULE.\n"                               \
  "\n"                                                                   \
  "MODULE is a user-ptr returned from swordmacs-module-open.\n"          \
  "A string containing the module's name is returned."

#define DOC_MODULE_KEY                                                   \
  "(swordmacs-module-key MODULE [KEY])\n"                                \
  "\n"                                                                   \
  "Get the text representation of the modules current key.\n"            \
  "\n"                                                                   \
  "MODULE is s user-ptr returned from swordmacs-module-open. If KEY\n"   \
  "is given, then the modules key is set to KEY first and then\n"        \
  " returned. The return value is a string."

#define DOC_MODULE_TEXT_STRIP                                            \
  "(swordmacs-module-text-strip MODULE)\n"                               \
  "\n"                                                                   \
  "Get the plain text version of the modules current entry.\n"           \
  "\n"                                                                   \
  "MODULE is a user-ptr returned from swordmacs-module-open.\n"

#define DOC_MODULE_SET_POSITION                                          \
  "(swordmacs-module-position MODULE POSITION)\n"                        \
  "\n"                                                                   \
  "Set the position of the module.\n"                                    \
  "\n"                                                                   \
  "MODULE is a user-ptr returned from swordmacs-module-open.\n"          \
  "POSITION is either 'top or 'bottom."

#define DOC_MODULE_INCREMENT                                             \
  "(swordmacs-module-inc MODULE)\n"                                      \
  "\n"                                                                   \
  "Increment the module's key by one.\n"                                 \
  "\n"                                                                   \
  "MODULE is a user-ptr returned from swordmacs-module-open.\n"

int plugin_is_GPL_compatible;

namespace swordmacs {

  static sword::SWMgr manager;
  static std::vector<const sword::SWText*> module_list;

  static emacs_value error;
  static emacs_value fset;
  static emacs_value list;
  static emacs_value nil;

  class SignaledError : public std::exception {
  public:
    SignaledError() : exception() { }
  };

  static bool
  non_local_exit(emacs_env *env) {
    return env->non_local_exit_check(env) != emacs_funcall_exit_return;
  }

  static void
  signal_error(emacs_env *env, const char *message) {
    emacs_value args[1];
    args[0] = env->make_string(env, message, strlen(message));
    auto data = env->funcall(env, list, 1, args);
    env->non_local_exit_signal(env, error, data);
  }

  static void
  signal_unhandled_error(emacs_env *env) {
    signal_error(env, "Unhandled error occured");
  }

  static ptrdiff_t
  get_string_size(emacs_env *env, const emacs_value val) {
    ptrdiff_t size{ 0 };
    env->copy_string_contents(env, val, NULL, &size);
    if (non_local_exit(env)) {
      throw SignaledError();
    }
    return size;
  }

  static std::string
  copy_string(emacs_env *env, const emacs_value val) {
    ptrdiff_t size = get_string_size(env, val);
    char buffer[size];
    bool success = env->copy_string_contents(env, val, buffer, &size);
    if (!success) {
      signal_error(env, "Failed to copy string");
      throw SignaledError();
    } else if (non_local_exit(env)) {
      throw SignaledError();
    }
    return buffer;
  }

  template <typename T>
  static emacs_value
  make_string_inner(emacs_env *env, const T text) {
    return env->make_string(env, text.c_str(), text.size());
  }

  template <>
  emacs_value
  make_string_inner(emacs_env *env, const char *text) {
    return env->make_string(env, text, strlen(text));
  }

  template <typename T>
  static emacs_value
  make_string(emacs_env *env, const T text) {
    auto string = make_string_inner(env, text);
    if (non_local_exit(env)) {
      throw SignaledError();
    }
    return string;
  }

  static void
  module_finalize(void*) noexcept {}

  template<typename T>
  static bool
  unknown_user_ptr(const std::vector<const T*> list, const T* module) {
    return (std::none_of(list.cbegin(), list.cend(),
                         [&](auto i){ return i == module; }));
  }

  static void
  maybe_add_module_to_list(const sword::SWText *module) {
    if (unknown_user_ptr(module_list, module)) {
      module_list.push_back(module);
    }
  }

  static bool
  is_bible(const sword::SWModule *module) noexcept {
    return !strcmp(module->getType(), sword::SWMgr::MODTYPE_BIBLES);
  }

  template <typename T>
  static emacs_value
  make_list(emacs_env *env, T &args) {
    auto value = env->funcall(env, list, args.size(), args.data());
    if (non_local_exit(env)) {
      throw SignaledError();
    }
    return value;
  }

  static emacs_value
  list_modules(emacs_env *env, ptrdiff_t, emacs_value[], void*) noexcept {
    try {
      std::vector<emacs_value> args;
      args.reserve(manager.Modules.size());
      std::array<emacs_value, 3> inner_args;
      for (const auto &pair: manager.Modules) {
        auto module = pair.second;
        if (is_bible(module)) {
          inner_args[0] = make_string(env, module->getName());
          inner_args[1] = make_string(env, module->getDescription());
          inner_args[2] = make_string(env, module->getLanguage());
          args.push_back(make_list(env, inner_args));
        }
      }
      return make_list(env, args);
    } catch(const SignaledError&) {
    } catch(...) {
      signal_unhandled_error(env);
    }
    return nil;
  }

  static emacs_value
  module_open(emacs_env *env, ptrdiff_t, emacs_value args[], void*) noexcept {
    try {
      auto name = copy_string(env, args[0]).c_str();
      auto module = manager.getModule(name);
      if (nullptr == module) {
        signal_error(env, "Undefined module name");
      } else {
        if (!is_bible(module)) {
          signal_error(env, "Swordmacs only supports Bible modules");
        } else {
          maybe_add_module_to_list(static_cast<sword::SWText*>(module));
          return env->make_user_ptr(env, module_finalize, module);
        }
      }
    } catch(const SignaledError&){
    } catch(...) {
      signal_unhandled_error(env);
    }
    return nil;
  }

  static auto
  module_cast(emacs_env *env, const emacs_value val) {
    auto module = static_cast<sword::SWText*>(env->get_user_ptr(env, val));
    if (non_local_exit(env)) {
      throw SignaledError();
    }
    if (unknown_user_ptr(module_list, module)) {
      signal_error(env, "Invalid user-ptr");
      throw SignaledError();
    }
    return module;
  }

  static emacs_value
  module_name(emacs_env *env, ptrdiff_t, emacs_value args[], void*) noexcept {
    try {
      auto module = module_cast(env, args[0]);
      return make_string(env, module->getName());
    } catch(const SignaledError&) {
    } catch(...) {
      signal_unhandled_error(env);
    }
    return nil;
  }

  static emacs_value
  module_key
  (emacs_env *env, ptrdiff_t nargs, emacs_value args[], void*) noexcept {
    try {
      auto module = module_cast(env, args[0]);
      if (2 == nargs) {
        auto key = copy_string(env, args[1]).c_str();
        module->setKey(key);
      }
      return make_string(env, module->getKey()->getText());
    } catch(const SignaledError&) {
    } catch(...) {
      signal_unhandled_error(env);
    }
    return nil;
  }

  static emacs_value
  module_text_strip
  (emacs_env *env, ptrdiff_t, emacs_value args[], void*) noexcept {
    try {
      auto module = module_cast(env, args[0]);
      return make_string(env, module->stripText());
    } catch(const SignaledError&) {
    } catch(...) {
      signal_unhandled_error(env);
    }
    return nil;
  }

  static emacs_value
  module_set_position
  (emacs_env *env, ptrdiff_t, emacs_value args[], void*) noexcept {
    try {
      auto module = module_cast(env, args[0]);
      auto position = args[1];
      auto bottom = env->intern(env, "bottom");
      auto top = env->intern(env, "top");
      if (env->eq(env, position, bottom)) {
        module->setPosition(sword::BOTTOM);
      } else if (env->eq(env, position, top)) {
        module->setPosition(sword::TOP);
      } else {
        signal_error(env, "Invalid position: valid values are 'top or 'bottom");
      }
    } catch(const SignaledError&) {
    } catch(...) {
      signal_unhandled_error(env);
    }
    return nil;
  }

  static emacs_value
  module_increment
  (emacs_env *env, ptrdiff_t, emacs_value args[], void*) noexcept {
    try {
      module_cast(env, args[0])->increment();
    } catch(const SignaledError&) {
    } catch(...) {
      signal_unhandled_error(env);
    }
    return nil;
  }

  static void
  set_function(emacs_env *env,
               const char *symbol,
               ptrdiff_t min_arity,
               ptrdiff_t max_arity,
               emacs_value (*function) (emacs_env*,
                                        ptrdiff_t,
                                        emacs_value[],
                                        void*) noexcept,
               const char *documentation) {
    emacs_value args[2];
    args[0] = env->intern(env, symbol);
    args[1] = env->make_function(env, min_arity, max_arity, function,
                                 documentation, 0);
    env->funcall(env, fset, 2, args);
  }

  extern "C" int
  emacs_module_init(struct emacs_runtime *ert) {
    auto *env = ert->get_environment(ert);

    /* Intern global symbols */
    error = env->intern(env, "error");
    fset = env->intern(env, "fset");
    list = env->intern(env, "list");
    nil = env->intern(env, "nil");

    /* Bind functions */
    set_function(env, "swordmacs-list-modules", 0, 0, list_modules,
                 DOC_LIST_MODULES);
    set_function(env, "swordmacs-module-open", 1, 1, module_open,
                 DOC_MODULE_OPEN);
    set_function(env, "swordmacs-module-name", 1, 1, module_name,
                 DOC_MODULE_NAME);
    set_function(env, "swordmacs-module-key", 1, 2, module_key,
                 DOC_MODULE_KEY);
    set_function(env, "swordmacs-module-text-strip", 1, 1, module_text_strip,
                 DOC_MODULE_TEXT_STRIP);
    set_function(env, "swordmacs-module-position", 2, 2, module_set_position,
                 DOC_MODULE_SET_POSITION);
    set_function(env, "swordmacs-module-inc", 1, 1, module_increment,
                 DOC_MODULE_INCREMENT);

    /* Provide feature */
    auto provide = env->intern(env, "provide");
    auto feature = env->intern(env, "swordmacs");
    env->funcall(env, provide, 1, &feature);

    return 0;
  }
}
