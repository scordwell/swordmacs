#include "emacs-module.h"

int plugin_is_GPL_compatible;

static void
null_user_ptr_finalize(void *user_ptr) {
  (void)user_ptr;
}

static emacs_value
null_user_ptr(emacs_env *env, ptrdiff_t nargs, emacs_value args[], void *data) {
  (void)nargs;
  (void)args;
  (void)data;
  return env->make_user_ptr(env, null_user_ptr_finalize, NULL);
}

int

emacs_module_init(struct emacs_runtime *ert) {
  emacs_env *env = ert->get_environment(ert);

  /* Bind functions */
  emacs_value fset = env->intern(env, "fset");
  emacs_value args[2];
  args[0] = env->intern(env, "swordmacs-testing-null-user-ptr");
  args[1] = env->make_function(env, 0, 0, null_user_ptr, "", 0);
  env->funcall(env, fset, 2, args);

  /* Provide feature */
  emacs_value provide = env->intern(env, "provide");
  emacs_value feature = env->intern(env, "swordmacs-testing");
  env->funcall(env, provide, 1, &feature);

  return 0;
}
